<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="./css/style.css" rel="stylesheet" type="text/css">
	<title>つぶやき編集</title>
</head>
<body>
<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMesages">
				<ul>
					<c:forEach  var="errorMessage" items="${errorMessages}">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>


	<div class="form-area">
		<form action="edit" method="post">
			<input name="editmessage" value="${message.id}" type="hidden">
			つぶやき<br />
			<textarea name="text" id="text" cols="100" rows="5" class="tweet-box"><c:out  value="${message.text}" /></textarea><br />
			<input type="submit" value="更新">
			<a href="./">戻る</a>
		</form>
	</div>

</div>

</body>
</html>