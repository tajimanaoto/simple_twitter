<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="./css/style.css" rel="stylesheet" type="text/css">
        <title>簡易Twitter</title>
    </head>
    <body>
        <div class="main-contents">
            <div class="header">
              	<c:if test="${ empty loginUser }">
        			<a href="login">ログイン</a>
        			<a href="signup">登録する</a>
    			</c:if>
    			<c:if test="${ not empty loginUser }">
        			<div class="profile">
        				<a href="./">ホーム</a>
       					<a href="setting">設定</a>
        				<a href="logout">ログアウト</a>

        				<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
        				<div class="account">@<c:out value="${loginUser.account}" /></div>
        				<div class="description"><c:out value="${loginUser.description}" /></div>
   					</div>
    			</c:if>
            </div>

            <c:if test="${ not empty errorMessages}">
            	<div class="errorMessages">
            		<c:forEach items="${errorMessages}" var="errorMessage">
            			<li><c:out value="${errorMessage}" />
            		</c:forEach>
            	</div>
            	<c:remove var="errorMessages" scope="session" />
            </c:if>

			<div class="date">
				<form action="index.jsp" method="get">
				日付：
				<input type="date" name="start" value="${strat}">
				～
				<input type="date" name="end" value="${end}">
				<input type="submit" value="検索">
				</form>
			</div>
            <div class="form-area">
            	<c:if test="${ isShowMessageForm }">
            		<form action="message" method="post">
            			いま、どうしてる？<br />
            			<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
            			<br />
            			<input type="submit" value="つぶやく">（140文字まで）
            		</form>
            	</c:if>
            </div>

			<div class="messages">
    			<c:forEach items="${messages}" var="message">
        			<div class="message">
            			<div class="account-name">
            				<!-- 実践課題②  ユーザーのつぶやきだけ表示する-->
                			<span class="account">
                				<a href="./?user_id=<c:out value="${message.userId}"/> ">
        							<c:out value="${message.account}" />
   								 </a>
                			</span>
                			<span class="name"><c:out value="${message.name}" /></span>
            			</div>

            			<!-- 改行されるようにfn:splitを追加 -->
            			<c:forEach var="s" items="${fn:split(message.text, '
            				')}">
           				 		<div class="text"><c:out value="${s}" /></div>
           				 </c:forEach>

            			<div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

						<!-- ログインユーザーであればボタンを表示 -->
						<c:if test="${ loginUser.id == message.userId }">
            				<form action="edit"method="get">
            					<input type="hidden" name="messageId" value="${message.id}">
            					<input type="submit" value="編集">
            				</form>
						</c:if>
						<c:if test="${ loginUser.id == message.userId }">
	            			<form action="deleteMessage" method="post">
	            				<input type="hidden" name="MessageId" value="${message.id}">
	            				<input type="submit" value="削除">
	            			</form>
	            		</c:if>
        			</div>

        			<div class="comment-form">
            			<c:if test="${ isShowMessageForm }">
            				<form action="comment" method="post">
            					<textarea name="text" cols="100" rows="5" class="comment-box"></textarea>
            					<br />
            					<input type="hidden" name="messageId" value="${message.id}">
								<input type="hidden" name="userId" value="${loginUser.id}">
            					<input type="submit" value="返信">
            				</form>
            			</c:if>
                	</div>

                	<c:forEach items="${comments}" var="comment">
						<c:if test="${message.id == comment.messageId }">
							<div class="comment">
								<div class="account-name">
									<span class="account"><c:out value="${comment.account}" /></span>
									<span class="name"><c:out value="${comment.name}" /></span>
								</div>
								<div class="comment-text"><c:out value="${comment.text}" /></div>
								<div class="date">
									<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
								</div>
							</div>
						</c:if>
					</c:forEach>

    			</c:forEach>
			</div>
            <div class="copyright"> TajimaNaoto</div>
        </div>
    </body>
</html>
