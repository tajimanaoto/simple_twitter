package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.MessageService;



@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet{


//	edit.jspにつぶやきの情報を表示させる
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException{
		HttpSession session = request.getSession();

//		今回はJSPでidの情報を指定しているのでgetParameterを使用
//		getParameterはString型で値が帰ってくる
		String editId = request.getParameter("messageId");
		Message message =null;

//		  IDが正しい（数字）ときだけ、実行する
		if (!(StringUtils.isBlank(editId)) && editId.matches("^[0-9]+$") ) {
			message = new MessageService().select(Integer.parseInt(editId));
		}

//		存在しないつぶやきのIDがURLに直接入力された場合のエラー表示
		if(message == null) {
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("不正なパラメーターが入力されました。");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		request.setAttribute("message", message);
        request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException{

		List<String> errorMessages = new ArrayList<String>();

		int editId = Integer.parseInt(request.getParameter("editmessage"));
		String text = request.getParameter("text");

		Message editmessage = new Message();
		editmessage.setId(editId);
		editmessage.setText(text);


		if(isValid (editmessage, errorMessages)) {
			try {
				new MessageService().update(editmessage);
			}catch(NoRowsUpdatedRuntimeException e) {
				 errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			}
		}

		if(errorMessages.size() != 0) {
			request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("message", editmessage);
            request.getRequestDispatcher("edit.jsp").forward(request, response);
            return;
		}

		response.sendRedirect("./");


	}

	protected Message getMessage(HttpServletRequest request)throws IOException, ServletException {

		Message message = new Message();

		message.setId(Integer.parseInt(request.getParameter("messageId")));
		message.setText(request.getParameter("text"));

		return message;
	}

	 private boolean isValid(Message message, List<String> errorMessages) {

		 String text = message.getText();

	    	// 空白か改行のみであるかどうか
	        if (StringUtils.isBlank(text)) {
	            errorMessages.add("メッセージを入力してください");
	        } else if (140 < text.length()) {
	            errorMessages.add("140文字以下で入力してください");
	        }
	        if (errorMessages.size() != 0) {
	            return false;
	        }
	        return true;
	    }

}
